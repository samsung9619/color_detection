import cv2
import numpy as np

cam = cv2.VideoCapture(1)

if not cam.isOpened():
  print ("Could not open cam")
  exit()

while(1):
    ret, frame = cam.read()
    if ret:
        frame = cv2.flip(frame,1)
        display = cv2.rectangle(frame,(200,100),(300,300),(0,255,0),2) 
        display = cv2.rectangle(frame,(350,100),(450,300),(255,0,0),2)
        display = cv2.rectangle(frame,(50,100),(150,300),(0,0,255),2)
        
        hsv = cv2.cvtColor(display,cv2.COLOR_BGR2HSV)
        
        lower_r1 = np.array([160,50,0])
        upper_r1 = np.array([179,255,255])  #red
        lower_g2 = np.array([63,30,70])
        upper_g2 = np.array([80,140,255])   #green
        lower_y4 = np.array([19,39,92])
        upper_y4 = np.array([35,163,255])   #yellow
        
        mask = cv2.inRange(hsv, lower_r1, upper_r1)
        red_blur = cv2.medianBlur(mask, 7)
        
        mask2 = cv2.inRange(hsv, lower_g2, upper_g2)
        green_blur = cv2.medianBlur(mask2, 7)
        
        mask4 = cv2.inRange(hsv, lower_y4, upper_y4)
        yellow_blur = cv2.medianBlur(mask4, 7)
        
        
        
        kernel1 = np.ones((3,3), np.uint8)
        kernel2 = np.ones((9,9), np.uint8)
        ero = cv2.erode(mask, kernel1, iterations = 1)
        dil = cv2.dilate(ero, kernel2, iterations = 1)
        
        ero1 = cv2.erode(mask2, kernel1, iterations = 1)
        dil1 = cv2.dilate(ero1, kernel2, iterations = 1)
        
        ero2 = cv2.erode(mask4, kernel1, iterations = 1)
        dil2 = cv2.dilate(ero, kernel2, iterations = 2)
        
        
        contours, hierarchy = cv2.findContours(dil1,  cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cv2.drawContours(frame, contours, -1, (255,0,0), 2)
        ROI2 = frame[100:300, 350:450].copy()
        
        contours, hierarchy = cv2.findContours(dil,  cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        cv2.drawContours(frame, contours, -1, (0,255,0), 2)
        ROI1 = frame[100:300, 200:300].copy()
        
        contours, hierarchy = cv2.findContours(dil2,  cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        cv2.drawContours(frame, contours, -1, (0,0,255), 2)
        ROI3 = frame[100:300, 50:150].copy()
        
        
        
        
        
        
        
        cv2.imshow('curFrame',frame)
        
        cv2.imshow('Current Roi', ROI1)
        cv2.imshow('Current Roi1', ROI2)
        cv2.imshow('Current Roi3', ROI3)
        #cv2.imshow('dil', dil)
        #cv2.imshow('dil1', dil1)
    if cv2.waitKey(10) & 0xFF == ord('q'):
        break

cam.release()
cv2.destroyAllWindows()