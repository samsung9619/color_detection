# -*- coding: utf-8 -*-
"""
Created on Fri Apr 23 09:20:11 2021

@author: samsung21040
============================
Feature:
1.This is version2.0 to color detection
Result:
1.Use OpenCV will detect sussceful line color
2.will recognize color
3.show pass/fail 20210503
============================
"""

#引入模組
import cv2
import numpy as np

#讀取影像檔
img  = cv2.imread('sp1.jpg')
img = cv2.resize(img,(720,720))
blur = cv2.GaussianBlur(img,(5,5),0)

#製作ROI區域
#ROI = blur[453:521, 190:587] #固定區域ROI使用
roi=cv2.selectROI(windowName="roi",img=blur,showCrosshair=False,fromCenter=False)
x,y,w,h=roi
cv2.rectangle(img=img,pt1=(x,y),pt2=(x+w,y+h),color=(255,255,0),thickness=1)
ROI=img[y:y+h,x:x+w]

hsv = cv2.cvtColor(ROI,cv2.COLOR_BGR2HSV) #RGB to HSV

#設定個顏色通道HSV值
lower_r1 = np.array([160,50,0])
upper_r1 = np.array([179,255,255])  #red

lower_g2 = np.array([63,30,70])
upper_g2 = np.array([80,140,255])   #green

lower_o3 = np.array([0,73,108])
upper_o3 = np.array([18,187,255])   #orange

lower_y4 = np.array([19,39,92])
upper_y4 = np.array([35,163,255])   #yellow

lower_w5 = np.array([0,0,148])
upper_w5 = np.array([132,32,233])   #white

lower_b6 = np.array([82,24,105])
upper_b6 = np.array([113,112,255])  #blue

#lower_bl7 = np.array([50,0,51])
#upper_bl7 = np.array([115,38,168])  #black


#顏色mask
mask1 = cv2.inRange(hsv, lower_r1, upper_r1)
red_blur = cv2.medianBlur(mask1, 7)

mask2 = cv2.inRange(hsv, lower_g2, upper_g2)
green_blur = cv2.medianBlur(mask2, 7)

mask3 = cv2.inRange(hsv, lower_o3, upper_o3)
orange_blur = cv2.medianBlur(mask3, 7)

mask4 = cv2.inRange(hsv, lower_y4, upper_y4)
yellow_blur = cv2.medianBlur(mask4, 7)

mask5 = cv2.inRange(hsv, lower_w5, upper_w5)
white_blur = cv2.medianBlur(mask5, 7)

mask6 = cv2.inRange(hsv, lower_b6, upper_b6)
blue_blur = cv2.medianBlur(mask6, 7)

#mask7 = cv2.inRange(hsv, lower_bl7, upper_bl7)
#blue_black = cv2.medianBlur(mask7, 7)          #黑色先不開啟

#mask疊加
mask = mask1 + mask2+ mask3 + mask4 + mask5 + mask6 

#mask噪點去除
kernel1 = np.ones((3,3), np.uint8)
ero = cv2.erode(mask, kernel1, iterations = 1)
kernel2 = np.ones((9,9), np.uint8)
dil = cv2.dilate(ero, kernel2, iterations = 2)


#判斷顏色   20210503
red_color = np.max(red_blur)
green_color = np.max(green_blur)
orange_color = np.max(orange_blur)
yellow_color = np.max(yellow_blur)
white_color = np.max(white_blur)
blue_color = np.max(blue_blur)

if red_color == 255:
        cv2.putText(img, "red", (20, 30), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255),2)
else:
    cv2.putText(img, "unfound red", (500, 30), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255),2)
if green_color == 255:
        cv2.putText(img, "green", (20, 60), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 255, 0),2)
else:
    cv2.putText(img, "unfound green", (500, 60), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 255, 0),2)
if orange_color == 255:
        cv2.putText(img, "orange", (20, 90), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 156, 255),2)
else:
    cv2.putText(img, "unfound orange" ,(500, 90), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 156, 255),2)
if yellow_color == 255:
        cv2.putText(img, "yellow", (20, 120), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 255, 255),2)
else:
    cv2.putText(img, "unfound yellow", (500, 120), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 255, 255),2)

if white_color == 255:
        cv2.putText(img, "white", (20, 150), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 255, 255),2)
else:
    cv2.putText(img, "unfound white", (500, 150), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 255, 255),2)
if blue_color == 255:
        cv2.putText(img, "blue", (20, 180), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 0),2)
else:
    cv2.putText(img, "unfound blue", (500, 180), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 0),2)

#判斷pass/fail  20210503
if red_color != 255 or green_color != 255 or orange_color != 255 or yellow_color != 255 or white_color!=255 or blue_color != 255:
    cv2.putText(img, "fail", (340, 680), cv2.FONT_HERSHEY_COMPLEX, 2, (0, 0, 255),3)
if red_color == 255 and green_color == 255 and orange_color == 255 and yellow_color == 255 and white_color==255 and blue_color == 255:
    cv2.putText(img, "pass", (310, 680), cv2.FONT_HERSHEY_COMPLEX, 2, (0, 255, 0),3)

'''if red_color == 255:
        cv2.putText(img, "red", (20, 30), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255),2)
else:
    cv2.putText(img, "unfound red", (500, 30), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 0, 255),2)
if green_color == 255:
        cv2.putText(img, "green", (20, 60), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 255, 0),2)
else:
    cv2.putText(img, "unfound green", (500, 60), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 255, 0),2)
if orange_color == 255:
        cv2.putText(img, "orange", (20, 90), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 156, 255),2)
else:
    cv2.putText(img, "unfound orange" ,(500, 90), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 156, 255),2)
if yellow_color == 255:
        cv2.putText(img, "yellow", (20, 120), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 255, 255),2)
else:
    cv2.putText(img, "unfound yellow", (500, 120), cv2.FONT_HERSHEY_COMPLEX, 1, (0, 255, 255),2)
if white_color == 255:
        cv2.putText(img, "white", (20, 150), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 255, 255),2)
else:
    cv2.putText(img, "unfound white", (500, 150), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 255, 255),2)
if blue_color == 255:
        cv2.putText(img, "blue", (20, 180), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 0),2)
else:
    cv2.putText(img, "unfound blue", (500, 180), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 0, 0),2)
'''
#找出顏色區塊圈出
contours, hierarchy = cv2.findContours(dil,  cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
cv2.drawContours(ROI, contours, -1, (0,255,0), 2)

#顯示影像

#cv2.imshow('blue',dil)
#cv2.imshow('mask',mask)
#cv2.imshow('con',ero)
#cv2.imshow('img',img)
cv2.namedWindow('img', cv2.WINDOW_NORMAL)
cv2.imshow('img', img)
#cv2.namedWindow('sp1ROI', cv2.WINDOW_NORMAL) #可縮放視窗
#cv2.imshow('sp1ROI', ROI)


cv2.waitKey(0)
cv2.destroyAllWindows() #關閉所有視窗