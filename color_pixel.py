# -*- coding: utf-8 -*-
"""
Created on Fri Apr 23 10:29:37 2021

@author: samsung21040
"""

import cv2
img = cv2.imread('sp1.jpg')
img = cv2.resize(img,(720,720))
type(img)

def onmouse(event, x, y, flags, param):
    if event==cv2.EVENT_MOUSEMOVE:
        print(img[y,x])

def main():
    cv2.namedWindow("img", cv2.WINDOW_NORMAL)
    cv2.setMouseCallback('img', onmouse)
    while True:
        cv2.imshow('img',img)
        if cv2.waitKey() == ord('q'):
            break
        cv2.destoryAllWindows()

if __name__ == '__main__':
    main()
    