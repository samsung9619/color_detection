# -*- coding: utf-8 -*-
"""
Created on Fri Apr 23 10:59:10 2021

@author: samsung21040
"""
import cv2
src=cv2.imread('sp1.jpg')

cv2.namedWindow('roi',cv2.WINDOW_NORMAL)

cv2.imshow('roi',src)

roi=cv2.selectROI(windowName="roi",img=src,showCrosshair=True,fromCenter=False)

x,y,w,h=roi

cv2.rectangle(img=src,pt1=(x,y),pt2=(x+w,y+h),color=(255,0,0),thickness=5)

img=src[y:y+h,x:x+w]

cv2.imshow('img',img)

cv2.waitKey(0)

cv2.destroyAllWindows()